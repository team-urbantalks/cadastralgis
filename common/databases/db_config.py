from django.conf import settings

db_configs = {}

GIS_DB_CONF = {
	'host' : settings.DATABASE_SERVER_IP,
    'port' : '5432',
	'database' : 'cadastralgis_gis',
	'user' : 'postgres',
	'password' : 'postgres'
}

db_configs = {
	'gis_db' : GIS_DB_CONF
}

#def init_pools():
#	for i in db_config:
#		db_pools[i] = init_db(db_config[i])

#init_pools()