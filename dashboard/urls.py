from django.urls import path
from django.conf.urls import url, include

from . import views
from django.contrib.auth import views as authviews

urlpatterns = [
    url(r'^auth/', include('social_django.urls', namespace='social')),
    url(r'^accounts/login/', views.app_login, name='login'),
    url(r'^logout/$', authviews.LogoutView.as_view(), name='logout'),    

    url(r'^parcel-map/$', views.parcel_map, name='parcel_map'), 

    url(r'', views.home, name='home'), 

]