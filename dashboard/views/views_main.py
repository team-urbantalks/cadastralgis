from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect, HttpResponseForbidden
from django.template import loader
import json
from django.conf import settings

from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.views import LoginView

def app_login(request):
	return LoginView.as_view(template_name='login.html')(request)

@login_required
def home(request):
	template = loader.get_template('index.html')
	return HttpResponse(template.render(None, request))


@login_required
@user_passes_test(lambda u: u.groups.filter(name='activated').exists(), login_url='/' )
def parcel_map(request):
	template = loader.get_template('parcel-map.html')
	return HttpResponse(template.render(None, request))
