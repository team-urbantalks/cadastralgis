from django.urls import path
from django.conf.urls import url, include
from rest_framework.documentation import include_docs_urls

from . import views  

urlpatterns = [
    #APIs
    url(r'^test/$', views.test.as_view()),    
    url(r'^list-api/$', views.list_api.as_view()),   
    url(r'^parcel-api/$', views.parcel_api.as_view()),      

    # Documentation
    url(r'^', include_docs_urls(title='Cadastral API')) 
]