from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions

from django.utils.decorators import method_decorator 
from django.contrib.auth.decorators import login_required, user_passes_test

from rest_framework.exceptions import ValidationError

import time

from common.databases import querydb

def user_is_activated(request):
    return request.user.groups.filter(name='activated').exists()


parcel_tables = {
    '27' : 'maharashtra_parcel'
}



class parcel_api(APIView):
    def get(self, request): 
        # start clock
        start = time.time()

        if user_is_activated(request):            
            # Query data
            query_dict = request.GET.dict()
            query_dict_ = {}
            for x in query_dict:
                val = query_dict[x].strip()
                if val is not None and val is not '':
                    query_dict_[x] = query_dict[x].strip()
            query_dict = query_dict_

            if query_dict['request_type'] == 'list':

                if 'parcel' not in query_dict:
                    response_to_send = {
                        "message": "'parcel' param is missing"
                    }
                    return Response(response_to_send)

                if 'village_code' not in query_dict:
                    response_to_send = {
                        "message": "'subdistrict_code' param is missing"
                    }
                    return Response(response_to_send)

                if 'state_code' not in query_dict:
                    response_to_send = {
                        "message": "'state_code' param is missing"
                    }
                    return Response(response_to_send)

                if query_dict['state_code'] in parcel_tables: 
                    parcel_table = parcel_tables[query_dict['state_code']]
                    query = f"""
                        WITH query AS (
                            SELECT 
                                1 as gid,

                                dis_name as dis_name,
                                sta_name as sta_name,
                                subdis_name as subdis_name,
                                vil_name as vil_name,

                                area as area,
                                perimeter as perimeter,
                                pin as pin,
                                wkb_geometry as geom,
                                ST_AsText(ST_Centroid(ST_Transform(wkb_geometry, 4326))) as point
                            FROM
                                public.{parcel_table}
                            WHERE
                                vil_code = '{ query_dict['village_code'] }'
                                AND
                                pin = '{ query_dict['parcel'] }'
                        )                        
                    """

                    geojson_builder_query = f"""
                        {query}
                        SELECT jsonb_build_object(
                            'type',     'FeatureCollection',
                            'features', jsonb_agg(feature)
                        ) as geojson
                        FROM (
                            SELECT jsonb_build_object(
                                    'type',       'Feature',
                                    'id',         gid,
                                    'geometry',   ST_AsGeoJSON(geom)::jsonb,
                                    'properties', to_jsonb(row) - 'gid' - 'geom'
                            ) as feature
                            FROM query row
                        ) features;
                    """
                    parcel_data = querydb(geojson_builder_query, 'gis_db')
                    parcel_data = parcel_data[0]

                else:
                    response_to_send = {
                        "message": f"No 'Parcel Table' found for supplied 'state_code' {query_dict['state_code']}"
                    }
                    return Response(response_to_send)  
            
            elif query_dict['request_type'] == 'xy':

                if 'xy' not in query_dict:
                    response_to_send = {
                        "message": "'xy' param is missing"
                    }
                    return Response(response_to_send)
                    
                parcel_data = get_parcel_by_xy(query_dict['xy'])
            
            elif query_dict['request_type'] == 'search':

                if 'xy' not in query_dict or 'pin' not in query_dict:
                    response_to_send = {
                        "message": "'xy' param or 'pin' is missing"
                    }
                    return Response(response_to_send)
                    
                parcel_data = search_parcel(query_dict['pin'], query_dict['xy'])

            else:
                response_to_send = {
                    "message": f"This API needs a valid 'request_type' paramater in form of 'xy' or 'list' or 'search'"
                }
                return Response(response_to_send)  


            data = {
                'parcel_data': parcel_data,
                'ror': {}
            }
            response_to_send = {
                "parcel_data": data,
                "timer": time.time() - start,
                "message": "API works - Sandeep"
            }
                
        else:
            response_to_send = 'Acces Denied'

        # Finally attach data

        return Response(response_to_send)

def get_parcel_by_xy(_xy):
    print(_xy)
    xy = f"POINT({_xy.replace(',',  ' ')})"
    print(xy)
    query = f"""
        SELECT
            sta_code
        FROM
            public.india_revenue_village
        WHERE 
            ST_Intersects(ST_GeomFromText('{xy}', 4326), wkb_geometry)
        LIMIT 1
    """
    data = querydb(query, 'gis_db')
    if len(data) > 0:
        s = data[0]['sta_code']
        parcel_table = parcel_tables[s]
        query = f"""
            WITH query AS (
                SELECT 
                    1 as gid,

                    dis_name as dis_name,
                    sta_name as sta_name,
                    subdis_name as subdis_name,
                    vil_name as vil_name,

                    area as area,
                    perimeter as perimeter,
                    pin as pin,
                    wkb_geometry as geom,
                    ST_AsText(ST_Centroid(ST_Transform(wkb_geometry, 4326))) as point
                FROM
                    public.{parcel_table}
                WHERE
                    ST_Intersects(ST_GeomFromText('{xy}', 4326), wkb_geometry)
            )                        
        """

        geojson_builder_query = f"""
            {query}
            SELECT jsonb_build_object(
                'type',     'FeatureCollection',
                'features', jsonb_agg(feature)
            ) as geojson
            FROM (
                SELECT jsonb_build_object(
                        'type',       'Feature',
                        'id',         gid,
                        'geometry',   ST_AsGeoJSON(geom)::jsonb,
                        'properties', to_jsonb(row) - 'gid' - 'geom'
                ) as feature
                FROM query row
            ) features;
        """
        parcel_data = querydb(geojson_builder_query, 'gis_db')
        parcel_data = parcel_data[0]
        return parcel_data
    else:
        print(data)
        return False



def search_parcel(pin , _xy):
    print(_xy)
    xy = f"POINT({_xy.replace(',',  ' ')})"
    print(xy)
    query = f"""
        SELECT
            sta_code
        FROM
            public.india_revenue_village
        WHERE 
            ST_Intersects(ST_GeomFromText('{xy}', 4326), wkb_geometry)
        LIMIT 1
    """
    data = querydb(query, 'gis_db')
    if len(data) > 0:
        s = data[0]['sta_code']
        parcel_table = parcel_tables[s]
        query = f"""
            WITH 
            a AS (
                SELECT
                    vil_code
                FROM
                    public.{parcel_table}
                WHERE
                    ST_Intersects(ST_GeomFromText('{xy}', 4326), wkb_geometry)
            ),
            b AS (
                SELECT
                    wkb_geometry as geom
                FROM
                    public.india_revenue_village
                WHERE
                    ST_Intersects(ST_GeomFromText('{xy}', 4326), wkb_geometry)
            ),
            query AS (
                SELECT 
                    1 as gid,

                    p.dis_name as dis_name,
                    p.sta_name as sta_name,
                    p.subdis_name as subdis_name,
                    p.vil_name as vil_name,
                    p.vil_code as vil_code,

                    p.area as area,
                    p.perimeter as perimeter,
                    p.pin as pin,
                    p.wkb_geometry as geom,
                    ST_AsText(ST_Centroid(ST_Transform(p.wkb_geometry, 4326))) as point
                FROM
                    public.{parcel_table} p,
                    a,
                    b
                WHERE
                    (
                        p.vil_code = a.vil_code
                        OR
                        ST_Intersects(b.geom, p.wkb_geometry)
                    )
                    AND
                    pin = '{pin}'
            )                        
        """

        geojson_builder_query = f"""
            {query}
            SELECT jsonb_build_object(
                'type',     'FeatureCollection',
                'features', jsonb_agg(feature)
            ) as geojson
            FROM (
                SELECT jsonb_build_object(
                        'type',       'Feature',
                        'id',         gid,
                        'geometry',   ST_AsGeoJSON(geom)::jsonb,
                        'properties', to_jsonb(row) - 'gid' - 'geom'
                ) as feature
                FROM query row
            ) features;
        """
        parcel_data = querydb(geojson_builder_query, 'gis_db')
        parcel_data = parcel_data[0]
        return parcel_data
    else:
        print(data)
        return False

