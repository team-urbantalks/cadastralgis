from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions

from django.utils.decorators import method_decorator 
from django.contrib.auth.decorators import login_required, user_passes_test

from rest_framework.exceptions import ValidationError

import time

from common.databases import querydb

def user_is_activated(request):
    return request.user.groups.filter(name='activated').exists()


parcel_tables = {
    '27' : 'maharashtra_parcel'
}


class list_api(APIView):
    def get(self, request): 
        # start clock
        start = time.time()

        if user_is_activated(request):            
            # Query data
            query_dict = request.GET.dict()
            query_dict_ = {}
            for x in query_dict:
                val = query_dict[x].strip()
                if val is not None and val is not '':
                    query_dict_[x] = query_dict[x].strip()
            query_dict = query_dict_

            if 'level' not in query_dict:
                response_to_send = {
                    "message": "'level' Param is missing"
                }
                return Response(response_to_send)

            
            if query_dict['level'] == 'district':

                if 'state_code' not in query_dict:
                    response_to_send = {
                        "message": "'state_code' param is missing"
                    }
                    return Response(response_to_send)

                query = f"""
                    SELECT 
                        district_name as name,
                        dtcode11 as code 
                    FROM public.india_district
                    WHERE
                        stcode11 = '{ query_dict['state_code'] }'
                """
                list_data = querydb(query, 'gis_db')

                geom_query = f"""
                    SELECT
                        ST_AsGeoJSON(ST_Simplify(ST_Transform(wkb_geometry, 4326), .01)) as geom
                    FROM public.india_state
                    WHERE
                        stcode11 = '{ query_dict['state_code'] }'
                """
                geom = querydb(geom_query, 'gis_db')

            
            elif query_dict['level'] == 'subdistrict':

                if 'district_code' not in query_dict:
                    response_to_send = {
                        "message": "'district_code' param is missing"
                    }
                    return Response(response_to_send)

                query = f"""
                    SELECT 
                        block_name as name,
                        subdis_code as code
                    FROM public.india_subdistrict
                    WHERE
                        dtcode11 = '{ query_dict['district_code'] }'
                """
                list_data = querydb(query, 'gis_db')

                geom_query = f"""
                    SELECT
                        ST_AsGeoJSON(ST_Simplify(ST_Transform(wkb_geometry, 4326), .005)) as geom
                    FROM public.india_district
                    WHERE
                        dtcode11 = '{ query_dict['district_code'] }'
                """
                geom = querydb(geom_query, 'gis_db')

            
            elif query_dict['level'] == 'village':

                if 'subdistrict_code' not in query_dict:
                    response_to_send = {
                        "message": "'subdistrict_code' param is missing"
                    }
                    return Response(response_to_send)

                query = f"""
                    WITH q AS(
                        SELECT
                            stcode01 as state_code,
                            dtcode01 as district_code,
                            block_code as subdistrict_code
                        FROM
                            public.india_subdistrict
                        WHERE
                            subdis_code = '{ query_dict['subdistrict_code'] }'
                    )
                        
                    SELECT 
                        v.vil_name as name,
                        v.vil_code as code
                    FROM 
                        public.india_revenue_village v,
                        q
                    WHERE
                        v.sta_code = q.state_code
                        AND
                        v.dis_code = q.district_code
                        AND
                        v.subdis_code = q.subdistrict_code
                """
                list_data = querydb(query, 'gis_db')

                geom_query = f"""
                    SELECT
                        ST_AsGeoJSON(ST_Simplify(ST_Transform(wkb_geometry, 4326), .001)) as geom
                    FROM 
                        public.india_subdistrict
                    WHERE
                        subdis_code = '{ query_dict['subdistrict_code'] }'
                """
                geom = querydb(geom_query, 'gis_db')
            

            elif query_dict['level'] == 'parcel':

                if 'village_code' not in query_dict:
                    response_to_send = {
                        "message": "'subdistrict_code' param is missing"
                    }
                    return Response(response_to_send)

                if 'state_code' not in query_dict:
                    response_to_send = {
                        "message": "'state_code' param is missing"
                    }
                    return Response(response_to_send)

                if query_dict['state_code'] in parcel_tables: 
                    parcel_table = parcel_tables[query_dict['state_code']]
                    query = f"""
                        SELECT 
                            pin as name,
                            pin as code
                        FROM 
                            public.{ parcel_table }
                        WHERE
                            vil_code = '{ query_dict['village_code'] }'
                    """
                    list_data = querydb(query, 'gis_db')

                    geom_query = f"""
                        SELECT
                            ST_AsGeoJSON(wkb_geometry) as geom
                        FROM 
                            public.india_revenue_village
                        WHERE
                            vil_code = '{ query_dict['village_code'] }'
                    """
                    geom = querydb(geom_query, 'gis_db')

                else:
                    response_to_send = {
                        "message": f"No 'Parcel Table' found for supplied 'state_code' {query_dict['state_code']}"
                    }
                    return Response(response_to_send)  

            else:
                response_to_send = {
                    "message": "'level' Param is invalid"
                }
                return Response(response_to_send)


            data = {
                "list": list_data,
                "geom": geom
            }
            response_to_send = {
                "nav_data": data,
                "timer": time.time() - start,
                "message": "API works - Sandeep"
            }

                
        else:
            response_to_send = 'Acces Denied'

        # Finally attach data

        return Response(response_to_send)





