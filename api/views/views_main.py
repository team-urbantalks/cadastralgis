from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions

from rest_framework.exceptions import ValidationError

import time

class test(APIView):
    def get(self, request): 
        # start clock
        start = time.time()

        # Finally attach data
        response_to_send = {
            "data": 'data',
            "timer": time.time() - start,
            "message": "API works - Sandeep"
        }
        return Response(response_to_send)
