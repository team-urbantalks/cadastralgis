
//--------- Draw Controls start -------------
var drawmode = 0;
function leaflet_add_drawing_tools(map){
  var style = {
      "color": "#FF2D00",
      "weight": 2,
      "opacity": 1,
      "fillOpacity": 0.3,
      "fillColor": "#f00",
      "selectedColor": "#000000",
  }
  var drawnItems = new L.FeatureGroup();
  map.addLayer(drawnItems);
  var drawControl = new L.Control.Draw({
    draw: {
       polygon : {
         allowIntersection: false,
         showArea: true,
         metric: 'metric',
         shapeOptions: style,
         bubblingMouseEvents: false
       },
       polyline: {
         metric: 'metric',
         shapeOptions: style,
         bubblingMouseEvents: false
       },
       circle: false,
       circlemarker: false,
       marker: false,
       rectangle: false
     },
     edit: {
       featureGroup: drawnItems,
       poly : {
         allowIntersection : false
       }
     },
  });
  map.addControl(drawControl);
  map.on('draw:edited', function (e) {
      var layers = e.layers;
      layers.eachLayer(function (layer) {
          //do whatever you want; most likely save back to db
      });
  });
  // Truncate value based on number of decimals
  var _round = function(num, len) {
      return Math.round(num*(Math.pow(10, len)))/(Math.pow(10, len));
  };
  // Helper method to format LatLng object (x.xxxxxx, y.yyyyyy)
  var strLatLng = function(latlng) {
      return "("+_round(latlng.lat, 6)+", "+_round(latlng.lng, 6)+")";
  };
  // Generate popup content based on layer type
  // - Returns HTML string, or null if unknown object
  var getPopupContent = function(layer) {
      // Marker - add lat/long
      if (layer instanceof L.Marker || layer instanceof L.CircleMarker) {
          return strLatLng(layer.getLatLng());
      // Circle - lat/long, radius
      } else if (layer instanceof L.Circle) {
          var center = layer.getLatLng(),
              radius = layer.getRadius();
          return "Center: "+strLatLng(center)+"<br />"
                +"Radius: "+_round(radius, 2)+" m";
      // Rectangle/Polygon - area
      } else if (layer instanceof L.Polygon) {
          var latlngs = layer._defaultShape ? layer._defaultShape() : layer.getLatLngs(),
          area = L.GeometryUtil.geodesicArea(latlngs);
          area = formatunits(area, 'area', 1);
          return  '<label style="color: #fff;">Drawn Area: </label></br>' + area;
          //return "Area: "+L.GeometryUtil.readableArea(area, true);
      } else if (layer instanceof L.Polyline) {
          var latlngs = layer._defaultShape ? layer._defaultShape() : layer.getLatLngs(),
          distance = 0;
          if (latlngs.length < 2) {
              return  '<label style="color: #fff;">Drawn Distance: N/A </label></br>';
              //return "Drawn Distance: N/A";
          } else {
              for (var i = 0; i < latlngs.length-1; i++) {
                  distance += latlngs[i].distanceTo(latlngs[i+1]);
              }
              distance = formatunits(distance, 'distance', 1);
              return  '<label style="color: #fff;">Drawn Distance: </label></br>' + distance;

          }
      }
      return null;
  };
  // Object created - bind popup to layer, add to feature group
  map.on(L.Draw.Event.CREATED, function(event) {
      var layer = event.layer;
      var content = getPopupContent(layer);
      if (content !== null) {
          layer.bindPopup(content);
      }
      drawnItems.addLayer(layer);
  });
  // Object(s) edited - update popups
  map.on(L.Draw.Event.EDITED, function(event) {
      var layers = event.layers,
          content = null;
      layers.eachLayer(function(layer) {
          content = getPopupContent(layer);
          if (content !== null) {
              layer.setPopupContent(content);
          }
      });
  });
  map.on(L.Draw.Event.DRAWSTART, function (event) {
    drawmode = 1;
  });
  map.on(L.Draw.Event.EDITSTART, function (event) {
    drawmode = 1;
  });
  map.on(L.Draw.Event.DRAWSTOP, function (event) {
    drawmode = 0;
  });
  map.on(L.Draw.Event.EDITSTOP, function (event) {
    drawmode = 0;
  });
}
//--//--------- Draw Controls End ------------


//--------- Upload Controls start -------------
function leaflet_add_upload_controls(map){
    L.Control.FileLayerLoad.LABEL = `<img class="icon" src="${static_dir}images/folder.svg" alt="file icon" style="
    width: 100%;
    padding: .2rem;"/>`;
    var control = L.Control.fileLayerLoad({
        // Allows you to use a customized version of L.geoJson.
        // For example if you are using the Proj4Leaflet leaflet plugin,
        // you can pass L.Proj.geoJson and load the files into the
        // L.Proj.GeoJson instead of the L.geoJson.
        fitBounds: true,
        layer: L.geoJson,
        // See http://leafletjs.com/reference.html#geojson-options
        layerOptions: {
          style: {
            color:'red'
          }
        },
        // Add to map after loading (default: true) ?
        addToMap: true,
        // File size limit in kb (default: 1024) ?
        fileSizeLimit: 4096,
        // Restrict accepted file formats (default: .geojson, .json, .kml, and .gpx) ?
        formats: [
            '.geojson',
            '.kml'
        ]
    });
    control.addTo(map);
    control.loader.on('data:loaded', function (event) {
      // event.layer gives you access to the layers you just uploaded!
      // Add to map layer switcher
      layercontroller.addOverlay(event.layer, event.filename);
      //event.layer.
      var tablehead = '<b> Uploaded Filename: </b>' + event.filename + '</br>';
      event.layer.eachLayer(function(layer){
        //console.log(layer.feature.properties);
        var props = layer.feature.properties;
        var container = document.createElement('div');
        container.style.padding = "20px 15px";
        var table = container.appendChild(document.createElement('table'));
        table.className = "table-hover table-striped";
        table.style.width = '100%';
        table.setAttribute('border', '1');
        var tbody = table.appendChild(document.createElement('tbody'));
        for (var key in props) {
          if (!(['styleUrl', 'styleHash', 'stroke', 'stroke-opacity', 'stroke-width', 'fill', 'fill-opacity', 'shape_area', 'shape_length'].indexOf(key) > -1)){
            var tr = tbody.insertRow(-1);
            var td =  tr.insertCell('td');
            td.innerHTML = props[key];
            var td =  tr.insertCell('td');
            td.innerHTML = key;
          }
        }
        //console.log(table);
        layer.bindPopup(tablehead + container.outerHTML);
      });
    });
    control.loader.on('data:error', function (error) {
       // Do something usefull with the error!
       console.log(error);
    });
  } //--------- Upload Controls end -------------





















// Helping Function ---------


function formatunits(value, type, padded=0){
    var units;
    if (type == 'area'){
      var area = value;
         units = {
        kilometers: (area*0.000001).toFixed(2) + " Kilometer²",
        hectare: (area*0.0001).toFixed(2) + " Hectar(s)",
        acre: (area*0.0002471054).toFixed(2) + " Acre(s)",
        bighas: (area*0.00049422).toFixed(2) + " Bighas(s)",
        guntha: (area*0.01).toFixed(2) + " Guntha(s)",
        are: (area*0.01).toFixed(2) + "  Are(s)",
        meters: area.toFixed(2) + " Meter²",
        feet: (area*10.76391).toFixed(2) + " Feet²",
        yards: (area*1.19599).toFixed(2) + " Yards²",
        inches: (area*1550.00310).toFixed(2) + " Inches²",
      }
    } else if (type == 'distance'){
      var distance = value;
      var units = {
        feet: (distance*3.28084).toFixed(2) + " Feet(s)",
        inches: (distance*39.37008).toFixed(2) + " Inch(es)",
        inches: (distance*1.093613).toFixed(2) + " Yard(s)",
        meters: distance.toFixed(2) + " Meter(s)",
        kilometers: (distance*0.001).toFixed(2) + " KMeter(s)",
        miles: (distance*0.0006213712).toFixed(2) + " Mile(s)",
      }
  
    }
    var element = document.createElement("select");
    for (key in units){
      var option = document.createElement("option");
      $(option).text(units[key]);
      if (key == 'meters'){
        $(option).attr("selected", "selected");
      }
      $(element).append(option);
    }
    if (padded > 0){
      element.className = 'form-control';
      var container = document.createElement('div');
      container.className = 'form-group';
      $(container).css({"padding": "10px 10px", "margin-top": "15px", "margin-bottom": "0px", "background": "#fff" });
      var label = document.createElement('label');
      label.innerHTML = "Select Units: ";
      $(container).append(label);
      $(container).append(element);
      return container.outerHTML;
    } else {
      return element.outerHTML;
    }
  }